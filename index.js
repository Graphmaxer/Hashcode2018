const get_args = require('./get_args');
const algo = require('./algo');
// const get_closest_ride = require('./get_closest_ride');
const Car = require('./Car');

let data = get_args(process.argv[2]);

algo(data);

data.cars.forEach(car => {
	console.log([car.races_done.length, ... car.races_done].join(' '));
});