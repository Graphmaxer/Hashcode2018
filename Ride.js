const getDistance = require('./get_distance');

module.exports = class Ride {
    constructor(start_x, start_y, end_x, end_y, earliest_start, latest_finish, index) {
        this.start = {
            x: +start_x,
            y: +start_y,
        };
        this.end = {
            x: +end_x,
            y: +end_y,
        };
        this.earliest_start = +earliest_start;
        this.latest_finish = +latest_finish;
		this.current_ride = false;
		this.in_progress = false;
		this.is_done = false;
		this.index = index;
	}
	
    getClosestFreeCar(cars) {
        let closest = null;
        cars.forEach(car => {
		let dist = getDistance(this.start, car.pos);
		if (car.isFree() && (closest === null || getDistance(this.start, closest.pos) > dist))
            closest = car;
        });
        return (closest);
	}
	
    is_free() {
		return	(current_ride == true);
    }
}
