const getline = require("get-line");
const fs = require('fs');
const Ride = require('./Ride');
const Car = require('./Car');

module.exports = function(filename) {

    lines = fs.readFileSync(filename, 'utf-8').split('\n');

    let args = {};

    [
        args.nb_rows,
        args.nb_columns,
        args.nb_vehicles,
        args.nb_rides,
        args.nb_bonus,
        args.nb_steps
	] = lines[0].split(' ').map(e => +e);
	
	args.cars = []
	for (let i = 0; i < args.nb_vehicles; i++)
		args.cars[i] = new Car();

	args.rides = lines.splice(1, args.nb_rides).map((line, index) => new Ride(... line.split(' '), index));

    return args;
}