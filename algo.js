module.exports = function algo(all) {
	for (let i = 0; i < all.nb_steps; i++) {
		all.rides
		.filter(ride => ride.in_progress === false && ride.is_done === false)
		.forEach(ride => {
			let closest = ride.getClosestFreeCar(all.cars);
			if (closest) {
				closest.assignRace(ride);
				ride.in_progress = true;
			}

		});
		
		all.cars.forEach(car => car.goForwardToRace());
	}
    return (all);
}