const getDistance = require('./get_distance');

module.exports = class Car {
    constructor() {
        this.pos = {
            x: 0,
            y: 0
        };
		this.current_race = null;
		this.has_started_course = false;
        this.races_done = [];
    }

    isFree() {
        return (this.current_race === null);
    }

    getPos() {
        return (this.pos);
    }

    getClosestRide(pos, rides, step) {
        let closest = rides[0];
        rides.forEach(ride => {
            let dist = getDistance(this.pos, ride.start);
			if (ride.is_free() && getDistance(this.start, closest.pos) > dist &&
			dist > ride.earliest_start - step)
                closest = ride;
        });
		return (closest)
	}
	
    distanceToRide(ride) {
        return (getDistance(this.pos, ride.start));
	}
	
	assignRace(race) {
		this.current_race = race;
		if (race.start.x == this.pos.x && race.start.y == this.pos.y)
			this.has_started_course = true;
		else
			this.has_started_course = false;
	}

	goForwardToRace() {
		// console.log(JSON.stringify({car: this, current_race: this.current_race}, null, '  '));
		let target = (this.has_started_course) ? this.current_race.end : (this.current_race) ? this.current_race.start : this.pos;

		if (this.current_race) {
			if (target.x !== this.pos.x)
				target.x += (target.x < this.pos.x) ? 1 : -1;
			else if (target.y !== this.pos.y)
				target.y += (target.y < this.pos.y) ? 1 : -1;
			else {
				if (this.has_started_course == false) {
					this.has_started_course = true 
				} else {
					this.races_done.push(this.current_race.index);
					this.current_race.is_done === true;
					this.has_started_course = false;
					this.current_race = null;
				}
			}
		}
    }
}